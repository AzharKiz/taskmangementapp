
import React, {Fragment,Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Button,
  View,
  TouchableOpacity,
  Text,
  StatusBar,
} from 'react-native';


import RNAccountKit, { LoginButton } from 'react-native-facebook-account-kit'

class Login extends Component {
    static navigationOptions = {
        headerTitle: 'Login',
    };
    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
        RNAccountKit.configure({

            countryWhitelist: ['IN']
        })

    }
    buttonPress = () => {
        RNAccountKit.loginWithPhone()
            .then((token) => {
                if (!token) {
                    console.log('Login cancelled')
                } else {
                    console.log(`Logged with phone. Token: ${token}`)
                    this.props.navigation.navigate('HomeSrceen')
                }
            })
    }
    render() {

        return (

            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

                <Button
                    title="Login"
                    onPress={() => this.buttonPress()}
                />

            </View>
        )
    }

};

const styles = StyleSheet.create({

});

export default Login;
