import React, {Fragment,Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  FlatList,
  View,
  PixelRatio,
  TouchableOpacity,
  Image,
  Text,
  Switch,
  TextInput
} from 'react-native';
import firebase from 'react-native-firebase';

import ImagePicker from 'react-native-image-picker';
const ref = firebase.database().ref('User/');


class Home extends Component {

    static navigationOptions = {
        headerTitle: 'Dashboard',
    };
    constructor(props) {
        super(props)
        this.state = {
            task: '',
            statusValue: '',
            imgUrl: '',
            ImageSource: null,
            switch1Value: false,
            taskData: []
        }
    }

    componentDidMount() {
        var oldtaskData = [];
        ref.on('value', (childSnapshot) => {
            childSnapshot.forEach((doc) => {

                oldtaskData.push({
                    task: doc._value.task,
                    statusValue: doc._value.statusValue,
                    // imgUrl: doc._value.imgurl
                })

                this.setState({
                    taskData: oldtaskData
                })
                console.log("data", this.state.taskData)

            })
        })
    }

    buttonPress = (task, statusValue, imgurl) => {
        // const ref = firebase.database().ref('User/');
        //ref.update({ foo: 'bar' });
        ref.push({
            task,
            statusValue,
            // imgurl
        })
    }


    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        }

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            let source = { uri: response.uri };


            this.setState({

                ImageSource: source,
                imgUrl: source

            });

        });
    }
    toggleSwitch1 = (value) => {
        this.setState({
            switch1Value: value,
            statusValue: value
        })
        console.log('Switch 1 is: ' + value)
    }
    handleEmail = (text) => {
        this.setState({ task: text })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 0.20, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ flex: 1 }}>
                        <TextInput style={{
                            margin: 15,
                            height: 40,
                            borderColor: '#7a42f4',
                            borderWidth: 1
                        }}
                            underlineColorAndroid="transparent"
                            placeholder="Task"
                            placeholderTextColor="#9a73ef"
                            autoCapitalize="none"
                            onChangeText={this.handleEmail} />
                    </View>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <Text>{'Status'}</Text>
                        <Switch
                            onValueChange={this.toggleSwitch1}
                            value={this.state.switch1Value} />
                    </View>
                    <View style={{ flex: 1 }}>

                        <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>

                            <View style={{
                                borderRadius: 10,
                                width: 50,
                                height: 50,
                                borderColor: '#9B9B9B',
                                borderWidth: 1 / PixelRatio.get(),
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: '#CDDC39',
                            }}>

                                {this.state.ImageSource === null ? <Text>Select a Photo</Text> :
                                    <Image style={styles.ImageContainer} source={this.state.ImageSource} />
                                }

                            </View>

                        </TouchableOpacity>

                    </View>

                    <TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} onPress={() => {
                        this.buttonPress(this.state.task, this.state.statusValue, this.state.imgUrl)
                    }
                    }>
                        <Text>{'submit'}</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 0.80 }}>

                    <View style={{ flex: 1 }}>
                        <FlatList
                            data={this.state.taskData}
                            renderItem={({ item, index }) => {
                                return (
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                        <View style={{ flex: 1 }}>
                                            <Text>{item.task}</Text>
                                        </View>
                                        <View style={{ flex: 1, alignItems: 'center' }}>
                                            <Text>{'Status'}</Text>
                                            <Switch

                                                value={item.statusValue} />
                                        </View>
                                        <View style={{ flex: 1 }}>



                                            {/* <Image style={styles.ImageContainer} source={this.state.ImageSource} /> */}
                                            {/* <Text>{item.imgUrl}</Text> */}


                                        </View>

                                    </View>
                                )
                            }
                            }
                        />
                    </View>
                </View>
            </View>
        )
    }

}


const styles = StyleSheet.create({
 
});

export default Home;